exports.componentTemplate = (type = 'layout', name) => {
  switch (type) {
    case 'layout':
      return `import React from "react"

export const ${name} = () => {
  return (
    <section className="py-10">
      <div className="container">
        hello from ${name}!
      </div>
    </section>
  )
}
`
    case 'element':
      return `import React from "react"

export const ${name} = () => {
  return <div className="">${name}</div>
}
`
    case 'global':
      return `import React from "react"

export const ${name} = () => {
  return <div className="">${name}</div>
}
`

    default:
      return `Drat, looks like you didn't give ${name} a valid type.`
  }
}
